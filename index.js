var CROSS = 'X';
var ZERO = 'O';
var EMPTY = ' ';
var player = CROSS;
var counter = 0;
var gameOver = false;

startGame();
function startGame () {
  renderGrid(3);
  showMessage('');
  counter = 0;
  player = CROSS;
  gameOver = false;
  winCells = [];
  console.log('reset!');
}

function paintCell(winCells) {
  for (var i = 0; i < winCells.length; i++) {
    findCell(winCells[i][0], winCells[i][1]).style.color = "red";
  };
};

function checkWin() {

  function checkRow (row) {
    for (var i = 0; i < 3; i++) {
      if (findCell(row, i).textContent !== player) {
        return false;
      }
    };
    for (var i = 0; i < 3; i++) {
      winCells.push([row, i]);
    };
    return true;
  };
  function checkRows () {
    for (var i = 0; i < 3; i++) {
      if (checkRow(i)) {
        return true;
      }
    };
    return false;
  };
 

  function checkCol (col) {
    for (var i = 0; i < 3; i++) {
      if (findCell(i, col).textContent !== player) {
        return false;
      }
    };
    for (var i = 0; i < 3; i++) {
      winCells.push([i, col]);
    };
    return true;
  };
  function checkCols () {
    for (var i = 0; i < 3; i++) {
      if (checkCol(i)) {
        return true;
      }
    };
    return false;
  };

  function checkDiagLeft () {
    for (var i = 0; i < 3; i++) {
        if (findCell(i, 2-i).textContent !== player) {
          return false;
        };
    };
    for (var i = 0; i < 3; i++) {
      winCells.push([i, 2-i]);
    };
    return true;
  };
  function checkDiagRight () {
    for (var i = 0; i < 3; i++) {
        if (findCell(i, i).textContent !== player) {
          return false;
        };
    };
    for (var i = 0; i < 3; i++) {
      winCells.push([i, i]);
    };
    return true;
  };

  function checkDiags () {
      if (checkDiagLeft() || checkDiagRight()) {
        return true;
      };
    return false;
  };

  if (checkDiags()) {
    return true;
  }

  if (checkRows()) {
    return true;
  }

  if (checkCols()) {
    return true;
  }

  return false;
}

/* обработчик нажатия на клетку */
function cellClickHandler (row, col) {
  if (findCell(row, col).textContent === EMPTY && !gameOver) {
    renderSymbolInCell(player, row, col);
    counter++;
    if (checkWin()) {
      showMessage(`Это победа ${player}!`);
      gameOver = true;
      paintCell(winCells);
    }
    
    if (player === CROSS) {
      player = ZERO;
    } else {
      player = CROSS;
    };
    if (counter === 9) {
      showMessage("Победила дружба!");
    }
  };
}

/* обработчик нажатия на кнопку "Сначала" */
function resetClickHandler () {
  startGame();
}

/* Служебные фукнции для взаимодействия с DOM. Данные функции нельзя редактировать! */
/* Показать сообщение */
function showMessage(text) {
  var msg = document.querySelector('.message');
  msg.innerText = text
}

/* Нарисовать игровое поле заданного размера */
function renderGrid (dimension) {
  var container = getContainer();
  container.innerHTML = '';

  for (let i = 0; i < dimension; i++) {
    var row = document.createElement('tr');
    for (let j = 0; j < dimension; j++) {
      var cell = document.createElement('td');
      cell.textContent = EMPTY;
      cell.addEventListener('click', () => cellClickHandler(i, j));
      row.appendChild(cell);
    }
    container.appendChild(row);
  }
}

/* Нарисовать символ symbol в ячейку(row, col) с цветом color */
function renderSymbolInCell (symbol, row, col, color = '#333') {
  var targetCell = findCell(row, col);

  targetCell.textContent = symbol;
  targetCell.style.color = color;
}

function findCell (row, col) {
  var container = getContainer();
  var targetRow = container.querySelectorAll('tr')[row];
  return targetRow.querySelectorAll('td')[col];
}

function getContainer() {
  return document.getElementById('fieldWrapper');
}

/* Test Function */
/* Победа первого игрока */
function testWin () {
  clickOnCell(0, 2);
  clickOnCell(0, 0);
  clickOnCell(2, 0);
  clickOnCell(1, 1);
  clickOnCell(2, 2);
  clickOnCell(1, 2);
  clickOnCell(2, 1);
}

/* Ничья */
function testDraw () {
  clickOnCell(2, 0);
  clickOnCell(1, 0);
  clickOnCell(1, 1);
  clickOnCell(0, 0);
  clickOnCell(1, 2);
  clickOnCell(1, 2);
  clickOnCell(0, 2);
  clickOnCell(0, 1);
  clickOnCell(2, 1);
  clickOnCell(2, 2);
}

function clickOnCell (row, col) {
  findCell(row, col).click();
}
